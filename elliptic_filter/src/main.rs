extern crate image;

use std::env;
use image::{GenericImageView, ImageBuffer, Rgb};

fn main() {
    let args: Vec<String> = env::args().collect();
    let image = image::open(&args[1]).unwrap();
    elliptic_filter(&image, 1.1, 1.1, 5)
}

fn elliptic_filter(image: &image::DynamicImage, sharpness: f32, strength: f32, size: u32) {
    let filter_size: u32 = size;
    let (width, height) = image.dimensions();
    let mut filtered_image = ImageBuffer::new(width, height);

    for y in 0..height {
        for x in 0..width {
            let mut sum_red = 0.0;
            let mut sum_green = 0.0;
            let mut sum_blue = 0.0;
            let mut sum_weight = 0.0;

            let x_start = (x as i32 - filter_size as i32).max(0) as u32;
            let x_end = (x + filter_size).min(width - 1);
            let y_start = (y as i32 - filter_size as i32).max(0) as u32;
            let y_end = (y + filter_size).min(height - 1);

            for j in y_start..=y_end {
                for i in x_start..=x_end {
                    let delta_x = (i as i32 - x as i32) as f32;
                    let delta_y = (j as i32 - y as i32) as f32;
                    let weight = 1.0 / (1.0 + sharpness * (delta_x * delta_x + delta_y * delta_y)).powf(strength);

                    let pixel = image.get_pixel(i, j);
                    let red = pixel[0] as f32;
                    let green = pixel[1] as f32;
                    let blue = pixel[2] as f32;

                    sum_red += weight * red;
                    sum_green += weight * green;
                    sum_blue += weight * blue;
                    sum_weight += weight;
                }
            }

            let red = (sum_red / sum_weight) as u8;
            let green = (sum_green / sum_weight) as u8;
            let blue = (sum_blue / sum_weight) as u8;

            let pixel = Rgb([red, green, blue]);
            filtered_image.put_pixel(x, y, pixel);
        }
    }
    filtered_image.save(".\\image.jpg").unwrap();
}